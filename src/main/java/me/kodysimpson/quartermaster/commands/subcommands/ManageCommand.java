package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.commands.SubCommand;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.menu.standard.LocksListMenu;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ManageCommand extends SubCommand {

    @Override
    public String getName() {
        return "manage";
    }

    @Override
    public String getDescription() {
        return "Manage your locks";
    }

    @Override
    public String getSyntax() {
        return "/qm manage";
    }

    @Override
    public void perform(Player p, String args[]) {

     //   if (p.hasPermission("qm.manage")){
            //Get the player's LockManagerMenu
            PlayerMenuUtility playerMenuUtility = QuarterMaster.getPlayerMenuUtility(p);

            //Only show the manage menu if he/she has at least 1 lock
            if (LockUtils.getNumOfLocks(p) == 0) {
                p.sendMessage(ChatColor.GRAY + "You don't have any locks to manage.");
                p.sendMessage(ChatColor.GRAY + "Do /quartermaster for more help.");
            } else {
                //Display all of the locks to the player
                new LocksListMenu().open(p);
            }
       // }else{
        //    p.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
        //}



    }

}

