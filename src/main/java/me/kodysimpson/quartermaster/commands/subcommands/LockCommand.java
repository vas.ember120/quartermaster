package me.kodysimpson.quartermaster.commands.subcommands;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.commands.SubCommand;
import me.kodysimpson.quartermaster.menu.standard.NewLockMenu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import me.kodysimpson.quartermaster.utils.PlayerSettingsUtils;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Door;
import org.bukkit.entity.Player;

public class LockCommand extends SubCommand {

    @Override
    public String getName() {
        return "lock";
    }

    @Override
    public String getDescription() {
        return "Allows you to lock a block";
    }

    @Override
    public String getSyntax() {
        return "/qm lock";
    }

    @Override
    public void perform(Player p, String[] args) {
        Block target;

        //if (p.hasPermission("qm.lock")){
            if (PlayerSettingsUtils.getSetting("canMakeLocks", p.getUniqueId().toString())){

                if (LockUtils.unlimitedLocks()){ //See if everyone has unlimited locks, from config

                    processBlockTargeting(p);

                }else{ //unlimited locks not for everyone, see if the player has unlimited locks
                    if (PlayerSettingsUtils.getSetting("unlimitedLocks", p.getUniqueId().toString())){ //player has unlimited locks

                        processBlockTargeting(p);

                    }else if(LockUtils.getNumOfLocks(p) < QuarterMaster.getPlugin().getConfig().getLong("max-locks")){ //since the player does not have unlimited locks, see if they are within there allowed range

                        processBlockTargeting(p);

                    }else{
                        p.sendMessage(ChatColor.RED + "You cannot make any more locks. You are at your limit.");
                    }
                }

            }else if(!PlayerSettingsUtils.getSetting("canMakeLocks", p.getUniqueId().toString())){
                p.sendMessage(ChatColor.RED + "You cannot make locks at this time.");
            }
        //}else{
           // p.sendMessage(ChatColor.RED + "You do not have permission to make locks.");
        //}


    }

    private void processBlockTargeting(Player p) {
        Block target;
        PlayerMenuUtility playerMenuUtility = QuarterMaster.getPlayerMenuUtility(p);

        if (LockUtils.getLockableWorlds().contains(p.getWorld().getName())){
            if (!(p.getTargetBlockExact(5) == null)) {
                target = p.getTargetBlockExact(5);
                if (LockUtils.getLockableBlocks().contains(target.getType().toString())){
                    if (LockUtils.isCurrentlyLocked(target)) {
                        if (LockUtils.getWhoLocked(target).equals(p)){ //if the lock is owned by you
                            p.sendMessage(ChatColor.RED + "You have already locked this block.");
                        }else{
                            p.sendMessage(ChatColor.RED + "That block is already locked by " + ChatColor.YELLOW + LockUtils.getWhoLocked(target).getName() + ChatColor.RED + ".");
                        }
                    } else {

                        if (LockUtils.isConnectedLockedChest(target)){ //is double chest
                            p.sendMessage(ChatColor.BLUE + "That chest is already locked.");
                        }else if(LockUtils.isConnectedLockedDoorHalf(target)){
                            p.sendMessage(ChatColor.RED + "That door is already locked.");
                        }else{
                            //Store the block so we can access it in other classes when we need it
                            playerMenuUtility.setLockToCreate(target);

                            //Show Confirmation Menu
                            new NewLockMenu().open(p);
                        }

                    }
                }else if (p.getWorld().getBlockAt(target.getX(), target.getY() + 1, target.getZ()).getState().getBlockData() instanceof Door) {

                    //See if either halfs of the above door are locked
                    if (LockUtils.isCurrentlyLocked(p.getWorld().getBlockAt(target.getX(), target.getY() + 1, target.getZ())) || LockUtils.isCurrentlyLocked(p.getWorld().getBlockAt(target.getX(), target.getY() + 2, target.getZ()))) {
                        p.sendMessage(ChatColor.RED + "The door above this block is already locked.");
                    }else{
                        //Store the block so we can access it in other classes when we need it
                        playerMenuUtility.setLockToCreate(target);

                        //Show Confirmation Menu
                        new NewLockMenu().open(p);
                    }
                }else{
                    p.sendMessage(ChatColor.RED + "This block can not be locked.");
                }
            } else if (p.getTargetBlockExact(5) == null) {
                p.sendMessage(ChatColor.GRAY + "Look at something nearby.");
            }
        }else{
            p.sendMessage(ChatColor.RED + "You can not make locks in this world.");
        }
    }


}
