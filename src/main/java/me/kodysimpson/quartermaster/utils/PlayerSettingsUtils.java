package me.kodysimpson.quartermaster.utils;

import me.kodysimpson.quartermaster.QuarterMaster;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerSettingsUtils { //Handle the settings for players

    public static boolean hasSettingsEntry(String playerUUID){

        PreparedStatement preparedStatement = null;
        int count = 0;
        try {
            preparedStatement = QuarterMaster.getConnection().
                    prepareStatement("SELECT * FROM PlayerSettings WHERE PlayerUUID = ?");
            preparedStatement.setString(1, playerUUID);

            ResultSet result = preparedStatement.executeQuery();
            while(result.next()){
                count++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (count == 1){
            return true;
        }else{
            return false;
        }

    }

    public static void createPlayerSettings(String playerUUID){

        if (!hasSettingsEntry(playerUUID)){
            try {
                PreparedStatement preparedStatement = QuarterMaster.getConnection().
                        prepareStatement("INSERT INTO PlayerSettings(PlayerUUID, CanMakeLocks, UnlimitedLocks, UnlimitedFriends) VALUES ( ?, ?, ?, ?)");
                preparedStatement.setString(1, playerUUID);
                preparedStatement.setBoolean(2, true);
                preparedStatement.setBoolean(3, false);
                preparedStatement.setBoolean(4, false);

                preparedStatement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("Player already has a settings file");
        }

    }

    public static boolean getSetting(String settingName, String playerUUID){

        PreparedStatement preparedStatement;
        boolean settingValue = false;
        try {
            preparedStatement = QuarterMaster.getConnection().
                    prepareStatement("SELECT * FROM PlayerSettings WHERE PlayerUUID = ?");
            preparedStatement.setString(1, playerUUID);

            ResultSet result = preparedStatement.executeQuery();
            while(result.next()){
                settingValue = result.getBoolean(settingName);
            }
            return settingValue;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return settingValue;
    }

    public static void setSetting(String setting, boolean value, String playerUUID){

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection().
                    prepareStatement("UPDATE PlayerSettings SET " + setting + " = ? WHERE PlayerUUID = ?");
            preparedStatement.setBoolean(1, value);
            preparedStatement.setString(2, playerUUID);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static boolean hasUnlimitedFriends(){
        return QuarterMaster.getPlugin().getConfig().getBoolean("unlimited-friends");
    }

}
