package me.kodysimpson.quartermaster.utils;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.model.Lock;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.data.type.Door;
import org.bukkit.entity.Player;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class LockUtils { //Handle anything directly related to lock management

    public static void createNewLock(Player p, Block block){

        try {
            PreparedStatement preparedStatement = QuarterMaster.getConnection().
                    prepareStatement("INSERT INTO Locks(PlayerUUID, BlockType, X, Y, Z, WorldName, CreationDate) VALUES ( ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, p.getUniqueId().toString());
            preparedStatement.setString(2, block.getType().toString());
            preparedStatement.setInt(3, block.getX());
            preparedStatement.setInt(4, block.getY());
            preparedStatement.setInt(5, block.getZ());
            preparedStatement.setString(6, block.getWorld().getName());
            preparedStatement.setTimestamp(7, new Timestamp(new Date().getTime()));

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("New Lock Created");

    }

    public static boolean isCurrentlyLocked(Block block){

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE WorldName = ? AND X = ? AND Y = ? AND Z = ?");
            preparedStatement.setString(1, block.getWorld().getName());
            preparedStatement.setInt(2, block.getX());
            preparedStatement.setInt(3, block.getY());
            preparedStatement.setInt(4, block.getZ());

            ResultSet results = preparedStatement.executeQuery();
            while(results.next()){
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean isCurrentlyLocked(Location location){

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X = ? AND Y = ? AND Z = ? AND WorldName = ?");
            preparedStatement.setInt(1, (int) location.getX());
            preparedStatement.setInt(2, (int) location.getY());
            preparedStatement.setInt(3, (int) location.getZ());
            preparedStatement.setString(4, location.getWorld().getName());

            ResultSet results = preparedStatement.executeQuery();
            while(results.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static OfflinePlayer getWhoLocked(Block block){

        PreparedStatement preparedStatement;
        String uuid = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X = ? AND Y = ? AND Z = ? AND WorldName=?");
            preparedStatement.setInt(1, block.getX());
            preparedStatement.setInt(2, block.getY());
            preparedStatement.setInt(3, block.getZ());
            preparedStatement.setString(4, block.getWorld().getName());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                uuid = ownerOfLock.getString("PlayerUUID");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Bukkit.getOfflinePlayer(UUID.fromString(uuid));
    }

    public static OfflinePlayer getWhoLocked(Location location){

        PreparedStatement preparedStatement;
        String uuid = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X = ? AND Y = ? AND Z = ? AND WorldName=?");
            preparedStatement.setInt(1, location.getBlockX());
            preparedStatement.setInt(2, location.getBlockY());
            preparedStatement.setInt(3, location.getBlockZ());
            preparedStatement.setString(4, location.getWorld().getName());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                uuid = ownerOfLock.getString("PlayerUUID");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Bukkit.getOfflinePlayer(UUID.fromString(uuid));
    }

    public static OfflinePlayer getWhoLocked(String lockID){

        PreparedStatement preparedStatement;
        String uuid = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE LockID=?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                uuid = ownerOfLock.getString("PlayerUUID");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Bukkit.getOfflinePlayer(UUID.fromString(uuid));
    }

    public static Location getLockLocation(String lockID){

        Location lockLocation = null;
        try {
            PreparedStatement preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE LockID = ?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                lockLocation = new Location(Bukkit.getWorld(ownerOfLock.getString("WorldName")), ownerOfLock.getInt("X"), ownerOfLock.getInt("Y"), ownerOfLock.getInt("Z"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lockLocation;
    }

    public static me.kodysimpson.quartermaster.model.Lock getLock(String lockID){

        PreparedStatement preparedStatement;
        Lock lock = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE LockID = ?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                lock = new me.kodysimpson.quartermaster.model.Lock(lockID, ownerOfLock.getString("PlayerUUID"), ownerOfLock.getString("BlockType"), new Location(Bukkit.getWorld(ownerOfLock.getString("WorldName")), ownerOfLock.getInt("X"), ownerOfLock.getInt("Y"), ownerOfLock.getInt("Z")), ownerOfLock.getTimestamp("CreationDate"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lock;
    }

    public static Lock getLock(Location location){

        Lock lock = null;
        String worldName = location.getWorld().getName();

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X = ? AND Y = ? AND Z = ? AND WorldName = ?");
            preparedStatement.setInt(1, location.getBlockX());
            preparedStatement.setInt(2, location.getBlockY());
            preparedStatement.setInt(3, location.getBlockZ());
            preparedStatement.setString(4, worldName);

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                return new me.kodysimpson.quartermaster.model.Lock(String.valueOf(ownerOfLock.getInt("LockID")), ownerOfLock.getString("PlayerUUID"), ownerOfLock.getString("BlockType"), new Location(Bukkit.getWorld(ownerOfLock.getString("WorldName")), ownerOfLock.getInt("X"), ownerOfLock.getInt("Y"), ownerOfLock.getInt("Z")), ownerOfLock.getTimestamp("CreationDate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lock;
    }

    public static ArrayList<Lock> getLocksForPlayer(String uuid){

        PreparedStatement preparedStatement;
        ArrayList<Lock> locks = new ArrayList<>();
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE PlayerUUID=?");
            preparedStatement.setString(1, uuid);

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                locks.add(new me.kodysimpson.quartermaster.model.Lock(String.valueOf(ownerOfLock.getInt("LockID")), ownerOfLock.getString("PlayerUUID"), ownerOfLock.getString("BlockType"), new Location(Bukkit.getWorld(ownerOfLock.getString("WorldName")), ownerOfLock.getInt("X"), ownerOfLock.getInt("Y"), ownerOfLock.getInt("Z")), ownerOfLock.getTimestamp("CreationDate")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return locks;
    }

    public static String getLockType(String lockID){
        PreparedStatement preparedStatement;
        String type = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE LockID=?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                type = ownerOfLock.getString("BlockType");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return type;
    }

    public static void deleteLock(Block block){

        //Use the location provided to identify the lock
        String world = block.getWorld().getName();

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("DELETE FROM Locks WHERE WorldName = ? AND X = ? AND Y = ? AND Z = ?");
            preparedStatement.setString(1, world);
            preparedStatement.setInt(2, block.getX());
            preparedStatement.setInt(3, block.getY());
            preparedStatement.setInt(4, block.getZ());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Lock deleted from database");
    }

    //Delete lock given it's ID
    public static void deleteLock(String lockID){

        PreparedStatement preparedStatement;
        Location lockLocation = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("DELETE FROM Locks WHERE LockID=?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void addPlayerToLock(String lockID, Player playerToAdd){

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("INSERT INTO Access(LockID, PlayerUUID) VALUES (?, ?)");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());
            preparedStatement.setString(2, playerToAdd.getUniqueId().toString());

            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Added Player to Lock Access List");

    }

    public static void alterLockLocation(Block block, Location newLocation){

        PreparedStatement preparedStatement;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("UPDATE Locks SET X = ?, Y = ?, Z = ? WHERE X = ? AND Y = ? AND Z = ? AND WorldName = ?");
            preparedStatement.setInt(1, newLocation.getBlockX());
            preparedStatement.setInt(2, newLocation.getBlockY());
            preparedStatement.setInt(3, newLocation.getBlockZ());
            preparedStatement.setInt(4, block.getX());
            preparedStatement.setInt(5, block.getY());
            preparedStatement.setInt(6, block.getZ());
            preparedStatement.setString(7, block.getWorld().getName());

            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Location getConnectedLocation(Block b) {

            org.bukkit.block.data.type.Chest data = (org.bukkit.block.data.type.Chest) b.getBlockData();

            String side = data.getType().toString();
            String direction = ((org.bukkit.block.data.type.Chest) b.getBlockData()).getFacing().toString();

            //Location of the block it's connected to
            Location locationOther = b.getLocation();

            if (direction.equalsIgnoreCase("north")) {
                if (side.equalsIgnoreCase("left")) {
                    //if on left, x = x + 1
                    locationOther.setX(locationOther.getX() + 1);
                } else if (side.equalsIgnoreCase("right")) {
                    //if on right, x = x - 1
                    locationOther.setX(locationOther.getX() - 1);
                }
            } else if (direction.equalsIgnoreCase("south")) {
                if (side.equalsIgnoreCase("left")) {
                    //if on left, x = x - 1
                    locationOther.setX(locationOther.getX() - 1);
                } else if (side.equalsIgnoreCase("right")) {
                    //if on right, x = x + 1
                    locationOther.setX(locationOther.getX() + 1);
                }
            } else if (direction.equalsIgnoreCase("west")) {
                if (side.equalsIgnoreCase("left")) {
                    //if on left, z = z - 1
                    locationOther.setZ(locationOther.getZ() - 1);
                } else if (side.equalsIgnoreCase("right")) {
                    //if on right, z = z + 1
                    locationOther.setZ(locationOther.getZ() + 1);
                }
            } else if (direction.equalsIgnoreCase("east")) {
                if (side.equalsIgnoreCase("left")) {
                    //if on left, z = z + 1
                    locationOther.setZ(locationOther.getZ() + 1);
                } else if (side.equalsIgnoreCase("right")) {
                    //if on right, z = z - 1
                    locationOther.setZ(locationOther.getZ() - 1);
                }
            }
            return locationOther;
    }

    public static void removePlayerFromLock(String lockID, OfflinePlayer playerToRemove){

        PreparedStatement preparedStatement;
        Location lockLocation = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("DELETE FROM Access WHERE LockID=? AND PlayerUUID=?");
            preparedStatement.setInt(1, Integer.getInteger(lockID).intValue());
            preparedStatement.setString(2, playerToRemove.getUniqueId().toString());

            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<String> getLockableBlocks(){
        return QuarterMaster.getPlugin().getConfig().getStringList("lockable-blocks");
    }

    public static List<OfflinePlayer> getAccessListFromID(String lockID){

        PreparedStatement preparedStatement;
        ArrayList<OfflinePlayer> accessList = new ArrayList<>();
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Access WHERE LockID=?");
            preparedStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet players = preparedStatement.executeQuery();
            while(players.next()){
                accessList.add(Bukkit.getOfflinePlayer(UUID.fromString(players.getString("PlayerUUID"))));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accessList;
    }

    public static List<OfflinePlayer> getAccessListFromBlock(Block block){

        ArrayList<OfflinePlayer> accessList = new ArrayList<>();
        PreparedStatement preparedStatement;
        String lockID = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X=? AND Y=? AND Z=? AND WorldName=?");
            preparedStatement.setInt(1, block.getX());
            preparedStatement.setInt(2, block.getY());
            preparedStatement.setInt(3, block.getZ());
            preparedStatement.setString(4, block.getWorld().getName());

            ResultSet foundLock = preparedStatement.executeQuery();
            while(foundLock.next()){
                lockID = foundLock.getString("LockID");
            }
            return getAccessListFromID(lockID);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accessList;
    }

    public static List<OfflinePlayer> getAccessListFromLocation(Location location){

        ArrayList<OfflinePlayer> accessList = new ArrayList<>();
        PreparedStatement preparedStatement;
        String lockID = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X=? AND Y=? AND Z=? AND WorldName=?");
            preparedStatement.setInt(1, location.getBlockX());
            preparedStatement.setInt(2, location.getBlockY());
            preparedStatement.setInt(3, location.getBlockZ());
            preparedStatement.setString(4, location.getWorld().getName());

            ResultSet foundLock = preparedStatement.executeQuery();
            while(foundLock.next()){
                lockID = foundLock.getString("LockID");
            }
            return getAccessListFromID(lockID);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accessList;
    }

    public static long getNumOfLocks(Player p){

        PreparedStatement preparedStatement;
        Location lockLocation = null;
        int count = 0;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE PlayerUUID=?");
            preparedStatement.setString(1, p.getUniqueId().toString());

            ResultSet results = preparedStatement.executeQuery();
            while(results.next()){
                count++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }

    public static List<String> getLockableWorlds(){
        return QuarterMaster.getPlugin().getConfig().getStringList("worlds");
    }

    //Access the database and grab every player there
    public static ArrayList<OfflinePlayer> getPlayersWithLocks(){

        PreparedStatement preparedStatement = null;
        ArrayList<OfflinePlayer> playersList = new ArrayList<>();
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks");

            ResultSet players = preparedStatement.executeQuery();
            while(players.next()){
                playersList.add(Bukkit.getOfflinePlayer(UUID.fromString(players.getString("PlayerUUID"))));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return playersList;

    }

    //Determine whether the chest provided is connected to a currently locked chest
    public static boolean isConnectedLockedChest(Block block){
        //See if the block provided is already connected to another chest that is a lock

        boolean bob;

        BlockState state = block.getState();

        if (state instanceof Chest){
            Chest chest = (Chest) state;
            Inventory inventory = chest.getInventory();
            bob = (inventory instanceof DoubleChestInventory);

            if(bob){
                org.bukkit.block.data.type.Chest data = (org.bukkit.block.data.type.Chest) block.getBlockData();

                String side = data.getType().toString();
                String direction = ((org.bukkit.block.data.type.Chest) block.getBlockData()).getFacing().toString();

                //Location of the block it's connected to
                Location locationOther = block.getLocation();

                if (direction.equalsIgnoreCase("north")){
                    if (side.equalsIgnoreCase("left")){
                        //if on left, x = x + 1
                        locationOther.setX(locationOther.getX() + 1);
                    }else if(side.equalsIgnoreCase("right")){
                        //if on right, x = x - 1
                        locationOther.setX(locationOther.getX() - 1);
                    }
                }else if (direction.equalsIgnoreCase("south")){
                    if (side.equalsIgnoreCase("left")){
                        //if on left, x = x - 1
                        locationOther.setX(locationOther.getX() - 1);
                    }else if(side.equalsIgnoreCase("right")){
                        //if on right, x = x + 1
                        locationOther.setX(locationOther.getX() + 1);
                    }
                }else if (direction.equalsIgnoreCase("west")){
                    if (side.equalsIgnoreCase("left")){
                        //if on left, z = z - 1
                        locationOther.setZ(locationOther.getZ() - 1);
                    }else if(side.equalsIgnoreCase("right")){
                        //if on right, z = z + 1
                        locationOther.setZ(locationOther.getZ() + 1);
                    }
                } else if (direction.equalsIgnoreCase("east")){
                    if (side.equalsIgnoreCase("left")){
                        //if on left, z = z + 1
                        locationOther.setZ(locationOther.getZ() + 1);
                    }else if(side.equalsIgnoreCase("right")){
                        //if on right, z = z - 1
                        locationOther.setZ(locationOther.getZ() - 1);
                    }
                }

                //See if the block it's connected to is locked
                return isCurrentlyLocked(locationOther);

            }
        }
        return false;
    }

    public static boolean isConnectedLockedDoorHalf(Block block){

        //See if it's a door
        if (block.getState().getBlockData() instanceof Door){


            Door door = (Door) block.getState().getBlockData();

            Location newLocation = null;
            if (door.getHalf().toString().equalsIgnoreCase("bottom")){

                newLocation = block.getLocation().add(0, 1, 0);

            }else if(door.getHalf().toString().equalsIgnoreCase("top")){

                newLocation = block.getLocation().add(0, -1, 0);

            }

            //Get the location of the door half that IS locked

            if (LockUtils.isCurrentlyLocked(newLocation)){ //see if the location of the opposite half of the door is locked

                return true;

            }

        }

        return false;
    }

    public static Lock getConnectedLockFromDoorHalf(Block block){

            Door door = (Door) block.getState().getBlockData();

            Location newLocation = null;
            if (door.getHalf().toString().equalsIgnoreCase("bottom")){

                newLocation = block.getLocation().add(0, 1, 0);

            }else if(door.getHalf().toString().equalsIgnoreCase("top")){

                newLocation = block.getLocation().add(0, -1, 0);

            }

            //Use the location of the locked half to grab an instance of the Lock
            return LockUtils.getLock(newLocation);

    }

    public static OfflinePlayer getOwnerConnectedChest(Block block){
        org.bukkit.block.data.type.Chest data = (org.bukkit.block.data.type.Chest) block.getBlockData();

        String side = data.getType().toString();
        String direction = ((org.bukkit.block.data.type.Chest) block.getBlockData()).getFacing().toString();

        //Location of the block it's connected to
        Location locationOther = block.getLocation();

        if (direction.equalsIgnoreCase("north")){
            if (side.equalsIgnoreCase("left")){
                //if on left, x = x + 1
                locationOther.setX(locationOther.getX() + 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, x = x - 1
                locationOther.setX(locationOther.getX() - 1);
            }
        }else if (direction.equalsIgnoreCase("south")){
            if (side.equalsIgnoreCase("left")){
                //if on left, x = x - 1
                locationOther.setX(locationOther.getX() - 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, x = x + 1
                locationOther.setX(locationOther.getX() + 1);
            }
        }else if (direction.equalsIgnoreCase("west")){
            if (side.equalsIgnoreCase("left")){
                //if on left, z = z - 1
                locationOther.setZ(locationOther.getZ() - 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, z = z + 1
                locationOther.setZ(locationOther.getZ() + 1);
            }
        } else if (direction.equalsIgnoreCase("east")){
            if (side.equalsIgnoreCase("left")){
                //if on left, z = z + 1
                locationOther.setZ(locationOther.getZ() + 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, z = z - 1
                locationOther.setZ(locationOther.getZ() - 1);
            }
        }

        PreparedStatement preparedStatement;
        String uuid = null;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X=? AND Y=? AND Z=? AND WorldName=?");
            preparedStatement.setInt(1, locationOther.getBlockX());
            preparedStatement.setInt(2, locationOther.getBlockY());
            preparedStatement.setInt(3, locationOther.getBlockZ());
            preparedStatement.setString(4, locationOther.getWorld().getName());

            ResultSet ownerOfLock = preparedStatement.executeQuery();
            while(ownerOfLock.next()){
                uuid = ownerOfLock.getString("PlayerUUID");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Bukkit.getOfflinePlayer(UUID.fromString(uuid));
    }

    public static List<OfflinePlayer> getAccessListFromConnectedBlock(Block block){

        org.bukkit.block.data.type.Chest data = (org.bukkit.block.data.type.Chest) block.getBlockData();

        String side = data.getType().toString();
        String direction = ((org.bukkit.block.data.type.Chest) block.getBlockData()).getFacing().toString();

        //Location of the block it's connected to
        Location locationOther = block.getLocation();

        if (direction.equalsIgnoreCase("north")){
            if (side.equalsIgnoreCase("left")){
                //if on left, x = x + 1
                locationOther.setX(locationOther.getX() + 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, x = x - 1
                locationOther.setX(locationOther.getX() - 1);
            }
        }else if (direction.equalsIgnoreCase("south")){
            if (side.equalsIgnoreCase("left")){
                //if on left, x = x - 1
                locationOther.setX(locationOther.getX() - 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, x = x + 1
                locationOther.setX(locationOther.getX() + 1);
            }
        }else if (direction.equalsIgnoreCase("west")){
            if (side.equalsIgnoreCase("left")){
                //if on left, z = z - 1
                locationOther.setZ(locationOther.getZ() - 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, z = z + 1
                locationOther.setZ(locationOther.getZ() + 1);
            }
        } else if (direction.equalsIgnoreCase("east")){
            if (side.equalsIgnoreCase("left")){
                //if on left, z = z + 1
                locationOther.setZ(locationOther.getZ() + 1);
            }else if(side.equalsIgnoreCase("right")){
                //if on right, z = z - 1
                locationOther.setZ(locationOther.getZ() - 1);
            }
        }

        PreparedStatement findPlayerUUIDStatement;
        PreparedStatement findAccessListStatement;
        ArrayList<OfflinePlayer> accessList = new ArrayList<>();
        String lockID = null;
        try {
            findPlayerUUIDStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE X=? AND Y=? AND Z=? AND WorldName=?");
            findPlayerUUIDStatement.setInt(1, locationOther.getBlockX());
            findPlayerUUIDStatement.setInt(2, locationOther.getBlockY());
            findPlayerUUIDStatement.setInt(3, locationOther.getBlockZ());
            findPlayerUUIDStatement.setString(4, locationOther.getWorld().getName());

            ResultSet ownerOfLock = findPlayerUUIDStatement.executeQuery();
            while(ownerOfLock.next()){
                lockID = ownerOfLock.getString("LockID");
            }

            findAccessListStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Access WHERE LockID = ?");
            findAccessListStatement.setInt(1, Integer.valueOf(lockID).intValue());

            ResultSet players = findAccessListStatement.executeQuery();
            while(players.next()){
                accessList.add(Bukkit.getOfflinePlayer(UUID.fromString(players.getString("PlayerUUID"))));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accessList;
    }

    public static int getLocksCount(String playerUUID){

        PreparedStatement preparedStatement;
        int count = 0;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE PlayerUUID=?");
            preparedStatement.setString(1, playerUUID);

            ResultSet results = preparedStatement.executeQuery();
            while(results.next()){
                count++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }

    //get the amount of locks this player has in that world
    public static HashMap<String, Integer> getLocksPerWorldCount(String playerUUID){

        HashMap<String, Integer> countPerWorld = new HashMap<>();

        PreparedStatement preparedStatement;
        int count;
        try {
            preparedStatement = QuarterMaster.getConnection()
                    .prepareStatement("SELECT * FROM Locks WHERE WorldName=? AND PlayerUUID=?");
            ArrayList<String> worlds = (ArrayList<String>) getLockableWorlds();
            //loop through each world and get the amount of locks from that player
            for (int i = 0; i < worlds.size(); i++){
                count = 0;
                preparedStatement.setString(1, worlds.get(i));
                preparedStatement.setString(2, playerUUID);

                ResultSet locks = preparedStatement.executeQuery();
                while (locks.next()){
                    count++;
                }

                countPerWorld.put(worlds.get(i), count);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return countPerWorld;
    }

    public static boolean unlimitedLocks(){
        return QuarterMaster.getPlugin().getConfig().getBoolean("unlimited-locks");
    }
}
