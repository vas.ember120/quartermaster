package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PaginatedMenu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.model.Lock;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;

public class LocksListMenu extends PaginatedMenu {

    @Override
    public String getMenuName() {
        return "QM > Select Lock";
    }

    @Override
    public int getSlots() {
        return 54;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        //See if the block chosen from the menu is one of the valid lockable locks
        for (int i = 0; i < LockUtils.getLockableBlocks().size(); i++) {
            if (e.getCurrentItem().getType().equals(Material.valueOf(LockUtils.getLockableBlocks().get(i)))) {

                //Set the lock ID for the menu, so we know what lock to work with and can access it at any point
                playerMenuUtility.setLockID(e.getCurrentItem().getItemMeta().getPersistentDataContainer().get(new NamespacedKey(QuarterMaster.getPlugin(), "lockID"), PersistentDataType.STRING));

                new ManageLockMenu().open(p); //Open the management menu for that lock
            }
        }
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {

            p.closeInventory();

        }else if(e.getCurrentItem().getType().equals(Material.DARK_OAK_BUTTON)){
            if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Left")){
                if (page == 0){
                    p.sendMessage(ChatColor.GRAY + "You are on the first page.");
                }else{
                    page = page - 1;
                    super.open(p);
                }
            }else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Right")){
                if (!((index + 1) >= LockUtils.getLocksForPlayer(playerMenuUtility.getP().getUniqueId().toString()).size())){
                    page = page + 1;
                    super.open(p);
                }
            }
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        addMenuBorder();

        ArrayList<Lock> locks = LockUtils.getLocksForPlayer(playerMenuUtility.getP().getUniqueId().toString());

        if(locks != null && !locks.isEmpty()) {
            for(int i = 0; i < getMaxItemsPerPage(); i++) {
                index = getMaxItemsPerPage() * page + i;
                if(index >= locks.size()) break;

                ItemStack lockItem = new ItemStack(Material.valueOf(locks.get(index).getBlockType()), 1);
                ItemMeta lock_meta = lockItem.getItemMeta();
                lock_meta.setDisplayName(ChatColor.GREEN + locks.get(index).getBlockType() + " Lock");
                ArrayList<String> lore = new ArrayList<>();
                lore.add(ChatColor.GOLD + "-------------");
                lore.add(ChatColor.YELLOW + "Location:");

                //Get the location subdocument
                Location location = locks.get(index).getLocation();

                lore.add(ChatColor.AQUA + "  x: " + ChatColor.GREEN + location.getBlockX());
                lore.add(ChatColor.AQUA + "  y: " + ChatColor.GREEN + location.getBlockY());
                lore.add(ChatColor.AQUA + "  z: " + ChatColor.GREEN + location.getBlockZ());
                lore.add(ChatColor.AQUA + "World: " + ChatColor.GREEN + location.getWorld().getName());
                lore.add("Date Created: " + locks.get(index).getCreationDate().toString());
                lore.add(ChatColor.GOLD + "-------------");
                lock_meta.setLore(lore);

                lock_meta.getPersistentDataContainer().set(new NamespacedKey(QuarterMaster.getPlugin(), "lockID"), PersistentDataType.STRING, locks.get(index).getLockID());

                lockItem.setItemMeta(lock_meta);

                inventory.addItem(lockItem);
            }
        }
    }
}
