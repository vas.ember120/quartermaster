package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ConfirmAddMenu extends Menu {
    @Override
    public String getMenuName() {
        return "Confirm: Add Player";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new AccessManagerMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.EMERALD)) {
            //They clicked yes, so add player to access list

            //Take the array from the lock in the DB and make an arraylist
            ArrayList<OfflinePlayer> accessList = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromID(playerMenuUtility.getLockID());
            if (p.equals(playerMenuUtility.getPlayerToAdd())) {
                p.sendMessage(ChatColor.DARK_RED + "You cannot add yourself, silly.");
            } else if (!(accessList.contains(playerMenuUtility.getPlayerToAdd()))) {
                LockUtils.addPlayerToLock(playerMenuUtility.getLockID(), playerMenuUtility.getPlayerToAdd());

                //Inform both players of this access addition
                p.sendMessage(ChatColor.GREEN + "Added " + ChatColor.YELLOW + playerMenuUtility.getPlayerToAdd().getName() + ChatColor.GREEN + " to the lock.");
                playerMenuUtility.getPlayerToAdd().sendMessage(ChatColor.YELLOW + p.getName() + ChatColor.GREEN + " has just granted you access to one of their " + LockUtils.getLockType(playerMenuUtility.getLockID()).toLowerCase() + " locks.");

                //Revert back to the previous menu
                new AccessManagerMenu().open(p);
            } else {
                p.sendMessage(ChatColor.RED + "This player already has access.");
            }
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {
        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yes_meta = yes.getItemMeta();
        yes_meta.setDisplayName(ChatColor.GREEN + "Yes");
        ArrayList<String> yes_lore = new ArrayList<>();
        yes_lore.add(ChatColor.AQUA + "Would you like to add ");
        yes_lore.add(ChatColor.AQUA + "this player to your lock?");
        yes_meta.setLore(yes_lore);
        yes.setItemMeta(yes_meta);
        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta no_meta = no.getItemMeta();
        no_meta.setDisplayName(ChatColor.DARK_RED + "No");
        no.setItemMeta(no_meta);

        inventory.setItem(3, yes);
        inventory.setItem(5, no);

        setFillerGlass();
    }
}
