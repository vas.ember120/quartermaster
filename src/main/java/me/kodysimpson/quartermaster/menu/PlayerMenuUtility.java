package me.kodysimpson.quartermaster.menu;

import me.kodysimpson.quartermaster.model.Lock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class PlayerMenuUtility {

    private Menu lastMenu;

    private Player p;
    private String lockID;

    private Player playerToAdd;
    private UUID uuidToRemove;
    private UUID uuidToManage;

    //Use this to store the block that is about to be added as a lock so we can keep track of it between the lock command and the other classes
    private Block lockToCreate;
    private Lock lockToRemove;

    public PlayerMenuUtility(Player p) {
        this.p = p;
    }

    public ItemStack makeItem(Material material, String name, ArrayList<String> lore) {
        ItemStack is = new ItemStack(material);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }

    public ItemStack makeItem(Material material, String name) {
        ItemStack is = new ItemStack(material);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        is.setItemMeta(im);
        return is;
    }

    public Lock getLockToRemove() {
        return lockToRemove;
    }

    public void setLockToRemove(Lock lockToRemove) {
        this.lockToRemove = lockToRemove;
    }

    //Store a lock ID for reference
    public void setLockID(String lockID) {
        this.lockID = lockID;
    }

    public String getLockID() {
        return lockID;
    }

    public void setPlayerToAdd(Player playerToAdd) {
        this.playerToAdd = playerToAdd;
    }

    public void setUUIDToRemove(UUID uuidToRemove) {
        this.uuidToRemove = uuidToRemove;
    }

    public UUID getUUIDToManage() {
        return uuidToManage;
    }

    public void setUUIDToManage(UUID uuidToManage) {
        this.uuidToManage = uuidToManage;
    }

    public void setLockToCreate(Block block) {
        this.lockToCreate = block;
    }

    public Block getLockToCreate() {
        return this.lockToCreate;
    }

    public Player getPlayerToAdd() {
        return playerToAdd;
    }

    public UUID getUUIDToRemove() {
        return uuidToRemove;
    }

    public Player getP() {
        return p;
    }

    public Menu getLastMenu() {
        return lastMenu;
    }

    public void setLastMenu(Menu lastMenu) {
        this.lastMenu = lastMenu;
    }
}
