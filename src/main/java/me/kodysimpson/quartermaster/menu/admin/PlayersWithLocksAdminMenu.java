package me.kodysimpson.quartermaster.menu.admin;

import me.kodysimpson.quartermaster.QuarterMaster;
import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PaginatedMenu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import me.kodysimpson.quartermaster.utils.SkullCreator;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class PlayersWithLocksAdminMenu extends PaginatedMenu {

    @Override
    public String getMenuName() {
        return "QM Admin > Players With Locks";
    }

    @Override
    public int getSlots() {
        return 54;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
            playerMenuUtility.setUUIDToManage(UUID.fromString(e.getCurrentItem().getItemMeta().getPersistentDataContainer().get(new NamespacedKey(QuarterMaster.getPlugin(), "uuid"), PersistentDataType.STRING)));
            new ManagePlayerAdminMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            p.closeInventory();
        } else if(e.getCurrentItem().getType().equals(Material.DARK_OAK_BUTTON)){
            if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Left")){
                if (page == 0){
                    p.sendMessage(ChatColor.GRAY + "You are on the first page.");
                }else{
                    page = page - 1;
                    super.open(p);
                }
            }else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Right")){
                if (!((index + 1) >= LockUtils.getLocksForPlayer(playerMenuUtility.getP().getUniqueId().toString()).size())){
                    page = page + 1;
                    super.open(p);
                }
            }
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        addMenuBorder();

        //Retrieve all players with locks
        ArrayList<OfflinePlayer> players_with_locks = LockUtils.getPlayersWithLocks();

        if (!(players_with_locks.isEmpty())) {
            if (players_with_locks != null && !players_with_locks.isEmpty()) {
                for (int i = 0; i < getMaxItemsPerPage(); i++) {
                    index = getMaxItemsPerPage() * page + i;
                    if (index >= players_with_locks.size()) break;
                    OfflinePlayer p2 = players_with_locks.get(index);

                    ItemStack player = SkullCreator.itemFromUuid(p2.getUniqueId());
                    ItemMeta player_meta = player.getItemMeta();
                    player_meta.setDisplayName(p2.getName());
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.GREEN + "Locks: " + LockUtils.getLocksCount(p2.getUniqueId().toString()));
                    lore.add(ChatColor.RED + "Locks per World:");
                    lore.add(ChatColor.GOLD + "-----------");
                    HashMap<String, Integer> countperworld = LockUtils.getLocksPerWorldCount(p2.getUniqueId().toString());
                    ArrayList<String> worlds = (ArrayList<String>) LockUtils.getLockableWorlds();
                    for (int x = 0; x < countperworld.size(); x++) {
                        lore.add(ChatColor.GREEN + worlds.get(x) + ": " + ChatColor.YELLOW + countperworld.get(worlds.get(x)));
                    }
                    lore.add(ChatColor.GOLD + "-----------");
                    player_meta.setLore(lore);

                    //Set the uuid of the player into the head item
                    player_meta.getPersistentDataContainer().set(new NamespacedKey(QuarterMaster.getPlugin(), "uuid"), PersistentDataType.STRING, p2.getUniqueId().toString());

                    player.setItemMeta(player_meta);
                    inventory.addItem(player);
                }
            }
        }
    }
}
